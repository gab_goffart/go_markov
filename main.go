package main

import (
	"bufio"
	//"flag"
	"flag"
	"fmt"
	"os"
	"strings"
	"time"
)

func main() {
	start := time.Now()

	authors, ngram := parseArgs()

	c := make(chan *Liste)
	for _, dir := range authors {
		go Parse(dir, ngram, c)
	}

	index := len(authors)
	var lists []*Liste

	for list := range c {
		lists = append(lists, list)

		//syncing all the goroutines
		index--
		if index == 0 {
			close(c)
			break
		}
	}

	for _, list := range lists {
		li := list.sort()
		print("Le n-gramme le plus populaire de l'artiste ")
		print("\"" + list.auteur + "\" est ")
		print("\"" + li[0].mot + "\" avec ")
		print(li[0].count)
		println(" répétitions. ")
	}

	fmt.Println("Done parsing and sorting. took ", time.Since(start).String())
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func Parse(author string, ngram int, c chan *Liste) {
	fmt.Println("Parsing author : " + author)
	li := NewListe(author)

	path, _ := os.Getwd()
	path += "/Texts/" + author

	dir, err := os.Open(path)
	check(err)
	defer dir.Close()

	filenames, err := dir.Readdir(0)
	check(err)

	for _, file := range filenames {
		fmt.Println("\tParsing file: " + file.Name())

		filepath := path + "/" + file.Name()
		parseFile(filepath, ngram, li)
	}

	c <- li
}

func parseFile(filepath string, ngramlength int, li *Liste) {
	var ngram []string

	file, err := os.Open(filepath)
	check(err)
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		line := scanner.Text()
		line = format(line)
		for _, word := range strings.Split(line, " ") {
			if len(word) < 3 {
				continue
			}

			ngram = append(ngram, word)

			if len(ngram) > ngramlength-1 {
				li.add(strings.Join(ngram, " "))
				ngram = ngram[1:]
			}
		}
	}
}

func parseArgs() ([]string, int) {
	var auteurs []string
	auteur := flag.String("a", "", "If present, parses only this author directory in directory Texts/")
	all := flag.Bool("A", false, "If present, parses all authors directories in directory Texts/")
	length := flag.Int("n", 2, "Specifies the length of the n-gram")
	flag.Parse()

	if !*all && *auteur == "" {
		flag.PrintDefaults()
		panic("")
	}

	if *all && *auteur != "" {
		panic("-A and -a are mutually exclusive")
	}

	dir, err := os.Open("Texts")
	check(err)
	defer dir.Close()

	dirs, err := dir.Readdirnames(0)
	check(err)
	if *all {
		auteurs = dirs
	} else {
		for _, d := range dirs {
			if d == *auteur {
				auteurs = append(auteurs, d)
			}
		}
	}

	return auteurs, *length
}