mac: 
	GOOS=darwin go build -o mac/markov_parser

windows: 
	GOOS=dos go build -o windows/markov_parser.exe

linux: 
	GOOS=linux go build -o linux/markov_parser
