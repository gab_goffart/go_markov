package main

import "sort"

type Liste struct {
	auteur string
	mots   map[string]Mot
}

func NewListe(auteur string) *Liste {
	return &Liste{auteur: auteur, mots: make(map[string]Mot)}
}

func (l *Liste) add(mot string) {
	if w, ok := l.mots[mot]; ok {
		w.count += 1
		l.mots[mot] = w
	} else {
		l.mots[mot] = Mot{mot: mot, count: 1}
	}
}

func (l *Liste) sort() []Mot {
	var test []Mot

	for _, value := range l.mots {
		test = append(test, value)

	}

	sort.Slice(test, func(i, j int) bool { return test[i].count > test[j].count })

	return test
}
