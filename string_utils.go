package main

import (
	"strings"
)

const punc = ",./\\\"'?!:;[]{}|()-_\n\t"

func format(s string) string {
	for _, p := range punc {
		s = strings.ReplaceAll(s, string(p), " ")
	}
	return strings.ToLower(s)
}